# Simple SDRAM Controller for the DE10-Lite-Board

***This is an old project from a few years ago and currently not functional. The
idea is to change that and add a Wishbone b.4 interface to it, but it will still
be a learning project.***

The current interface directly drives the control and data lines of the
SDR-SDRAM chip and has the following interface:

```verilog
module sdram_controller(
  input               iclk,
  input               ireset,

  input                   iwrite_req,
  input           [21:0]  iwrite_address,
  input          [127:0]  iwrite_data,
  output                  owrite_ack,

  input                   iread_req,
  input           [21:0]  iread_address,
  output         [127:0]  oread_data,
  output                  oread_ack,
    
 //////////// SDRAM //////////
 output      [12:0]  DRAM_ADDR,
 output       [1:0]  DRAM_BA,
 output              DRAM_CAS_N,
 output              DRAM_CKE,
 output              DRAM_CLK,
 output              DRAM_CS_N,
 inout       [15:0]  DRAM_DQ,
 output              DRAM_LDQM,
 output              DRAM_RAS_N,
 output              DRAM_UDQM,
 output              DRAM_WE_N
);
```

The project includes the initialization routine and offers a simple
interface for quick prototyping and also handle the refreshing.
Currently it lacks any parametrization for fine tuning.

## TODO's

- [ ] Refactor to use localparams and less magic numbers
- [ ] Verify timings as it depends directly on the `50MHz` clock
- [ ] Choose a runner and add tests
  - [ ] Verify initialization process
  - [ ] Verify single reads
  - [ ] Verify single writes
  - [ ] Verify that auto refresh works
  - [ ] Verify sequences of above

## Future goals

- [ ] Add wishbone b4 interface
- [ ] Add some simple buffering/cache
- [ ] Add cdc to make the SDRAM clock independent.
- [ ] Add a `n` times wide databus and use burst read/writes. Eg. `2`*32 for
      a 64b bus interface
