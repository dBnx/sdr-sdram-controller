`default_nettype none

module wb_sdram_controller (
    // RCC
    input wire i_clk,
    input wire i_reset,
    input wire i_en,

    // Wishbone
    input  wire [31:0] i_wb_adr,
    input  wire [31:0] i_wb_dat,
    input  wire [ 3:0] i_wb_sel,
    input  wire        i_wb_we,
    input  wire        i_wb_cyc,
    input  wire        i_wb_stb,
    output reg  [31:0] o_wb_dat,
    output reg         o_wb_ack,

    //////////// SDRAM //////////
    output [12:0] DRAM_ADDR,
    output [ 1:0] DRAM_BA,
    output        DRAM_CAS_N,
    output        DRAM_CKE,
    output        DRAM_CLK,
    output        DRAM_CS_N,
    inout  [15:0] DRAM_DQ,
    output        DRAM_LDQM,
    output        DRAM_RAS_N,
    output        DRAM_UDQM,
    output        DRAM_WE_N
);

  //=======================================================
  //  REG/WIRE declarations
  //=======================================================
  reg     [127:0] sdr_data;
  wire    [ 21:0] sdr_address;
  reg             sdr_write_request;
  reg             sdr_read_request;
  wire            sdr_write_finished;
  wire            sdr_read_finished;
  wire    [127:0] sdr_write_data;
  wire    [127:0] sdr_read_data;

  //=======================================================
  //  Glue
  //=======================================================
  integer         i;
  always @(posedge i_clk) begin
    if (i_reset) begin
      o_wb_ack <= 1'b0;
      sdr_write_request <= 1'b0;
      sdr_read_request <= 1'b0;
    end else begin
      o_wb_ack <= 1'b0;
      sdr_write_request <= 1'b0;
      sdr_read_request <= 1'b0;

      if (i_en && i_wb_cyc & i_wb_stb & ~o_wb_ack) begin
        sdr_write_request <= i_wb_we;
        sdr_read_request <= ~i_wb_we;
        sdr_address <= i_wb_adr;
        sdr_write_data <= i_wb_dat;
        if (sdr_read_finished || sdr_write_finished) begin
          o_wb_ack <= 1'b1;
          if (~i_wb_we) begin
            o_wb_dat <= sdr_read_data;
          end
        end
      end

    end
  end

  //=======================================================
  //  Structural coding
  //=======================================================
  sdram_controller sdram_controller (
      .iclk  (i_clk),
      .ireset(i_reset),

      .iwrite_req(sdr_write_request),
      .iwrite_address(sdr_address),
      .iwrite_data(sdr_write_data),
      .owrite_ack(sdr_write_finished),

      .iread_req(sdr_read_request),
      .iread_address(sdr_address),
      .oread_data(sdr_read_data),
      .oread_ack(sdr_read_finished),

      //////////// SDRAM //////////
      .DRAM_ADDR(DRAM_ADDR),
      .DRAM_BA(DRAM_BA),
      .DRAM_CAS_N(DRAM_CAS_N),
      .DRAM_CKE(DRAM_CKE),
      .DRAM_CLK(DRAM_CLK),
      .DRAM_CS_N(DRAM_CS_N),
      .DRAM_DQ(DRAM_DQ),
      .DRAM_LDQM(DRAM_LDQM),
      .DRAM_RAS_N(DRAM_RAS_N),
      .DRAM_UDQM(DRAM_UDQM),
      .DRAM_WE_N(DRAM_WE_N)
  );

endmodule

// Reference SDR Ram controller 
// From:  https://github.com/Arkowski24/sdram-controller
module sdram_controller (
    input iclk,
    input ireset,

    input          iwrite_req,
    input  [ 21:0] iwrite_address,
    input  [127:0] iwrite_data,
    output         owrite_ack,

    input          iread_req,
    input  [ 21:0] iread_address,
    output [127:0] oread_data,
    output         oread_ack,

    //////////// SDRAM //////////
    output [12:0] DRAM_ADDR,
    output [ 1:0] DRAM_BA,
    output        DRAM_CAS_N,
    output        DRAM_CKE,
    output        DRAM_CLK,
    output        DRAM_CS_N,
    inout  [15:0] DRAM_DQ,
    output        DRAM_LDQM,
    output        DRAM_RAS_N,
    output        DRAM_UDQM,
    output        DRAM_WE_N
);

  //=======================================================
  //  REG/WIRE declarations
  //=======================================================
  reg  [ 8:0] state = 9'b000000001;
  reg  [ 8:0] next_state = 9'b000000001;
  reg  [ 2:0] mul_state = 3'b001;

  reg         read_ack = 1'b0;
  reg         write_ack = 1'b0;

  //Next opperation priority - 0 = Write, 1 = Read
  reg         next_prior = 1'b0;

  //SDRAM INITLIZE MODULE
  reg         init_ireq = 1'b0;
  wire        init_ienb;
  wire        init_fin;

  //SDRAM WRITE MODULE
  reg         write_ireq = 1'b0;
  wire        write_ienb;
  wire [12:0] write_irow;
  wire [ 9:0] write_icolumn;
  wire [ 1:0] write_ibank;
  wire        write_fin;

  //SDRAM READ MODULE
  reg         read_ireq = 1'b0;
  wire        read_ienb;
  wire [12:0] read_irow;
  wire [ 9:0] read_icolumn;
  wire [ 1:0] read_ibank;
  wire        read_fin;


  //=======================================================
  //  Structural coding
  //=======================================================
  assign {write_ibank, write_irow, write_icolumn} = {iwrite_address, 3'b0};
  assign {read_ibank, read_irow, read_icolumn}    = {iread_address, 3'b0};

  assign owrite_ack                               = write_ack;
  assign oread_ack                                = read_ack;

  assign {read_ienb, write_ienb, init_ienb}       = mul_state;

  always @(posedge iclk) begin
    if (ireset == 1'b1) state <= #1 9'b000000001;
    else state <= #1 next_state;
  end

  always @(state or init_fin or iwrite_req or iread_req or write_fin or read_fin or next_prior)
begin
    case (state)
      //Init States
      9'b000000001: next_state <= 9'b000000010;
      9'b000000010: if (init_fin) next_state <= 9'b000000100;
 else next_state <= 9'b000000010;

      //Idle State
      9'b000000100:
      if (next_prior) begin
        if (iread_req) next_state <= 9'b001000000;
        else if (iwrite_req) next_state <= 9'b000001000;
        else next_state <= 9'b000000100;
      end else begin
        if (iwrite_req) next_state <= 9'b000001000;
        else if (iread_req) next_state <= 9'b001000000;
        else next_state <= 9'b000000100;
      end
      //Write States
      9'b000001000: next_state <= 9'b000010000;
      9'b000010000: if (write_fin) next_state <= 9'b000100000;
 else next_state <= 9'b000010000;
      9'b000100000: next_state <= 9'b000000100;

      //Read States        `
      9'b001000000: next_state <= 9'b010000000;
      9'b010000000: if (read_fin) next_state <= 9'b100000000;
 else next_state <= 9'b010000000;
      9'b100000000: next_state <= 9'b000000100;
      default:      next_state <= 9'b000000001;
    endcase
  end

  always @(state) begin
    case (state)
      //Init States
      default,
      9'b000000001: begin
        init_ireq  <= 1'b1;
        write_ireq <= 1'b0;
        read_ireq  <= 1'b0;

        write_ack  <= 1'b0;
        read_ack   <= 1'b0;

        mul_state  <= 3'b001;
      end
      9'b000000010: begin
        init_ireq  <= 1'b0;
        write_ireq <= 1'b0;
        read_ireq  <= 1'b0;

        write_ack  <= 1'b0;
        read_ack   <= 1'b0;

        mul_state  <= 3'b001;
      end

      //Idle State
      9'b000000100: begin
        init_ireq  <= 1'b0;
        write_ireq <= 1'b0;
        read_ireq  <= 1'b0;

        write_ack  <= 1'b0;
        read_ack   <= 1'b0;

        mul_state  <= 3'b001;
      end

      //Write States
      9'b000001000: begin
        init_ireq  <= 1'b0;
        write_ireq <= 1'b1;
        read_ireq  <= 1'b0;

        write_ack  <= 1'b0;
        read_ack   <= 1'b0;

        mul_state  <= 3'b010;
      end

      9'b000010000: begin
        init_ireq  <= 1'b0;
        write_ireq <= 1'b0;
        read_ireq  <= 1'b0;

        write_ack  <= 1'b0;
        read_ack   <= 1'b0;

        mul_state  <= 3'b010;
      end
      9'b000100000: begin
        init_ireq  <= 1'b0;
        write_ireq <= 1'b0;
        read_ireq  <= 1'b0;

        write_ack  <= 1'b1;
        read_ack   <= 1'b0;

        mul_state  <= 3'b010;
        next_prior <= 1'b1;
      end

      //Read States
      9'b001000000: begin
        init_ireq  <= 1'b0;
        write_ireq <= 1'b0;
        read_ireq  <= 1'b1;

        write_ack  <= 1'b0;
        read_ack   <= 1'b0;

        mul_state  <= 3'b100;
      end
      9'b010000000: begin
        init_ireq  <= 1'b0;
        write_ireq <= 1'b0;
        read_ireq  <= 1'b0;

        write_ack  <= 1'b0;
        read_ack   <= 1'b0;

        mul_state  <= 3'b100;
      end
      9'b100000000: begin
        init_ireq  <= 1'b0;
        write_ireq <= 1'b0;
        read_ireq  <= 1'b0;

        write_ack  <= 1'b0;
        read_ack   <= 1'b1;

        mul_state  <= 3'b100;
        next_prior <= 1'b0;
      end
    endcase
  end

  sdram_initalize sdram_init (
      .iclk  (iclk),
      .ireset(ireset),

      .ireq(init_ireq),
      .ienb(init_ienb),

      .ofin(init_fin),

      .DRAM_ADDR(DRAM_ADDR),
      .DRAM_BA(DRAM_BA),
      .DRAM_CAS_N(DRAM_CAS_N),
      .DRAM_CKE(DRAM_CKE),
      .DRAM_CLK(DRAM_CLK),
      .DRAM_CS_N(DRAM_CS_N),
      .DRAM_DQ(DRAM_DQ),
      .DRAM_LDQM(DRAM_LDQM),
      .DRAM_RAS_N(DRAM_RAS_N),
      .DRAM_UDQM(DRAM_UDQM),
      .DRAM_WE_N(DRAM_WE_N)
  );

  sdram_write sdram_write (
      .iclk  (iclk),
      .ireset(ireset),

      .ireq(write_ireq),
      .ienb(write_ienb),

      .irow(write_irow),
      .icolumn(write_icolumn),
      .ibank(write_ibank),
      .idata(iwrite_data),
      .ofin(write_fin),

      .DRAM_ADDR(DRAM_ADDR),
      .DRAM_BA(DRAM_BA),
      .DRAM_CAS_N(DRAM_CAS_N),
      .DRAM_CKE(DRAM_CKE),
      .DRAM_CLK(DRAM_CLK),
      .DRAM_CS_N(DRAM_CS_N),
      .DRAM_DQ(DRAM_DQ),
      .DRAM_LDQM(DRAM_LDQM),
      .DRAM_RAS_N(DRAM_RAS_N),
      .DRAM_UDQM(DRAM_UDQM),
      .DRAM_WE_N(DRAM_WE_N)
  );

  sdram_read sdram_read (
      .iclk  (iclk),
      .ireset(ireset),

      .ireq(read_ireq),
      .ienb(read_ienb),

      .irow(read_irow),
      .icolumn(read_icolumn),
      .ibank(read_ibank),
      .odata(oread_data),
      .ofin(read_fin),

      .DRAM_ADDR(DRAM_ADDR),
      .DRAM_BA(DRAM_BA),
      .DRAM_CAS_N(DRAM_CAS_N),
      .DRAM_CKE(DRAM_CKE),
      .DRAM_CLK(DRAM_CLK),
      .DRAM_CS_N(DRAM_CS_N),
      .DRAM_DQ(DRAM_DQ),
      .DRAM_LDQM(DRAM_LDQM),
      .DRAM_RAS_N(DRAM_RAS_N),
      .DRAM_UDQM(DRAM_UDQM),
      .DRAM_WE_N(DRAM_WE_N)
  );

endmodule

// ----------------------------------------------------------- Initialize

//  SDRAM Settings
//  Write Bust      -- Single Location
//  CAS Latency     -- 2
//  Burst           -- Sequential 
//  Burst Length    -- 1
module sdram_initalize (
    input  iclk,
    input  ireset,
    input  ireq,
    input  ienb,
    output ofin,

    output        DRAM_CLK,
    output        DRAM_CKE,
    output [12:0] DRAM_ADDR,
    output [ 1:0] DRAM_BA,
    output        DRAM_CAS_N,
    output        DRAM_CS_N,
    output        DRAM_RAS_N,
    output        DRAM_WE_N,
    output        DRAM_LDQM,
    output        DRAM_UDQM,
    output [15:0] DRAM_DQ
);

  reg  [ 7:0] state = 8'b00000001;
  reg  [ 7:0] next_state;

  reg  [ 3:0] command = 4'h0;
  reg  [12:0] address = 13'h0;
  reg  [ 1:0] bank = 2'b00;
  reg  [ 1:0] dqm = 2'b11;

  reg         ready = 1'b0;

  reg  [15:0] counter = 16'h0;
  reg         ctr_reset = 0;

  wire        ref_cycles;
  wire        init_begin_counter;

  assign ofin                                           = ready;

  assign DRAM_ADDR                                      = ienb ? address : {32{1'bz}};
  assign DRAM_BA                                        = ienb ? bank : 2'bzz;
  assign {DRAM_CS_N, DRAM_RAS_N, DRAM_CAS_N, DRAM_WE_N} = ienb ? command : 4'bzzzz;
  assign {DRAM_UDQM, DRAM_LDQM}                         = ienb ? dqm : 2'bzz;
  assign DRAM_CLK                                       = ienb ? ~iclk : 1'bz;
  assign DRAM_CKE                                       = ienb ? 1'b1 : 1'bz;
  assign DRAM_DQ                                        = ienb ? 16'h0000 : {16{1'bz}};

  always @(posedge iclk or posedge ctr_reset) begin
    if (ctr_reset) counter <= #1 16'h0;
    else counter <= #1 (counter + 1'b1);
  end

  //ref_cycles > 16 - refresh, nop - 8 times
  assign ref_cycles = (counter >= 16);
  assign init_begin_counter = (counter >= 10000);
  //assign init_begin_counter = (counter >= 12);

  always @(posedge iclk) begin
    if (ireset == 1'b1) state <= #1 8'b00000001;
    else state <= #1 next_state;
  end

  always @(state or ireq or ref_cycles or init_begin_counter) begin
    case (state)
      //IDLE
      8'b00000001:
      if (ireq) next_state <= 8'b00000010;
      else next_state <= 8'b00000001;
      //NOP - POWER UP
      8'b00000010,
      default:
      if (init_begin_counter) next_state <= 8'b00000100;
      else next_state <= 8'b00000010;
      8'b00000100: next_state <= 8'b00001000;
      8'b00001000: next_state <= 8'b00010000;
      8'b00010000:
      if (ref_cycles) next_state <= 8'b00100000;
      else next_state <= 8'b00001000;
      8'b00100000: next_state <= 8'b01000000;
      8'b01000000: next_state <= 8'b10000000;
      8'b10000000: next_state <= 8'b10000000;
      default: next_state <= 8'b00000001;
    endcase
  end

  always @(state) begin
    case (state)
      8'b00000001: begin
        command   <= #1 4'b0111;
        address   <= #1 13'b0000000000000;
        bank      <= #1 2'b00;
        ready     <= #1 1'b0;

        ctr_reset <= #1 1'b1;
      end
      default,
      8'b00000010: begin
        command   <= #1 4'b0111;
        address   <= #1 13'b0000000000000;
        bank      <= #1 2'b00;
        ready     <= #1 1'b0;

        ctr_reset <= #1 1'b0;
      end
      8'b00000100: begin
        command   <= #1 4'b0010;
        address   <= #1 13'b0010000000000;
        bank      <= #1 2'b11;
        ready     <= #1 1'b0;

        ctr_reset <= #1 1'b1;
      end
      8'b00001000: begin
        command   <= #1 4'b0001;
        address   <= #1 13'b0000000000000;
        bank      <= #1 2'b00;
        ready     <= #1 1'b0;

        ctr_reset <= #1 1'b0;
      end
      8'b00010000: begin
        command   <= #1 4'b0111;
        address   <= #1 13'b0000000000000;
        bank      <= #1 2'b00;
        ready     <= #1 1'b0;

        ctr_reset <= #1 1'b0;
      end
      8'b00100000: begin
        command   <= #1 4'b0000;
        bank      <= #1 2'b00;
        address   <= #1 13'b0000000100011;
        ready     <= #1 1'b0;

        ctr_reset <= #1 1'b0;
      end
      8'b01000000: begin
        command   <= #1 4'b0111;
        bank      <= #1 2'b00;
        address   <= #1 13'b0000000000000;
        ready     <= #1 1'b0;

        ctr_reset <= #1 1'b0;
      end
      8'b10000000: begin
        command   <= #1 4'b0111;
        bank      <= #1 2'b00;
        address   <= #1 13'b0000000000000;
        ready     <= #1 1'b1;

        ctr_reset <= #1 1'b0;
      end
    endcase
  end

endmodule


// ----------------------------------------------------------- Read
module sdram_read (
    input  iclk,
    input  ireset,
    input  ireq,
    input  ienb,
    output ofin,

    input  [ 12:0] irow,
    input  [  9:0] icolumn,
    input  [  1:0] ibank,
    output [127:0] odata,

    output        DRAM_CLK,
    output        DRAM_CKE,
    output [12:0] DRAM_ADDR,
    output [ 1:0] DRAM_BA,
    output        DRAM_CAS_N,
    output        DRAM_CS_N,
    output        DRAM_RAS_N,
    output        DRAM_WE_N,
    output        DRAM_LDQM,
    output        DRAM_UDQM,
    input  [15:0] DRAM_DQ
);

  reg  [  7:0] state = 8'b00000001;
  reg  [  7:0] next_state;

  reg  [  3:0] command = 4'h0;
  reg  [ 12:0] address = 13'h0;
  reg  [  1:0] bank = 2'b00;
  reg  [127:0] data = 128'b0;
  reg  [  1:0] dqm = 2'b11;

  reg          ready = 1'b0;

  reg  [  7:0] counter = 8'h0;
  reg          ctr_reset = 0;

  wire         dqm_count;
  wire         data_count;

  assign ofin                                           = ready;
  assign odata                                          = data;

  assign DRAM_ADDR                                      = ienb ? address : {13{1'bz}};
  assign DRAM_BA                                        = ienb ? bank : 2'bzz;
  assign {DRAM_CS_N, DRAM_RAS_N, DRAM_CAS_N, DRAM_WE_N} = ienb ? command : 4'bzzzz;
  assign {DRAM_UDQM, DRAM_LDQM}                         = ienb ? dqm : 2'bzz;
  assign DRAM_CLK                                       = ienb ? ~iclk : 1'bz;
  assign DRAM_CKE                                       = ienb ? 1'b1 : 1'bz;

  always @(posedge iclk or posedge ctr_reset) begin
    if (ctr_reset) counter <= #1 8'h0;
    else counter <= #1 (counter + 1'b1);
  end

  assign dqm_count  = (counter < 5);
  assign data_count = (counter == 7);

  always @(posedge iclk) begin
    if (ireset == 1'b1) state <= #1 8'b00000001;
    else state <= #1 next_state;
  end

  always @(state or ireq or dqm_count or data_count) begin
    case (state)
      //IDLE
      8'b00000001: if (ireq) next_state <= 8'b00000010;
 else next_state <= 8'b00000001;
      //ACTIVE
      8'b00000010: next_state <= 8'b00000100;
      //NOP
      8'b00000100: next_state <= 8'b00001000;
      //READ
      8'b00001000: next_state <= 8'b00010000;
      //CAS - 1 NOP
      8'b00010000: next_state <= 8'b00100000;
      //CAS - 2 NOP
      8'b00100000: next_state <= 8'b01000000;
      //READING - 8
      8'b01000000: if (data_count) next_state <= 8'b10000000;
 else next_state <= 8'b01000000;
      //NOP - FIN
      8'b10000000: next_state <= 8'b00000001;
      default:     next_state <= 8'b00000001;
    endcase
  end

  always @(state) begin
    case (state)
      //IDLE
      default,
      8'b00000001: begin
        command   <= #1 4'b0111;
        address   <= #1 13'b0000000000000;
        bank      <= #1 2'b00;
        dqm       <= #1 2'b11;
        data      <= #1 data;
        ready     <= #1 1'b0;

        ctr_reset <= #1 1'b0;
      end
      //ACTIVE
      8'b00000010: begin
        command   <= #1 4'b0011;
        address   <= #1 irow;
        bank      <= #1 ibank;
        dqm       <= #1 2'b11;
        data      <= #1 data;
        ready     <= #1 1'b0;

        ctr_reset <= #1 1'b0;
      end
      //NOP
      8'b00000100: begin
        command   <= #1 4'b0111;
        address   <= #1 13'b0000000000000;
        bank      <= #1 2'b00;
        dqm       <= #1 2'b11;
        data      <= #1 data;
        ready     <= #1 1'b0;

        ctr_reset <= #1 1'b0;
      end
      //READ
      8'b00001000: begin
        command   <= #1 4'b0101;
        address   <= #1{3'b001, icolumn};
        bank      <= #1 ibank;
        dqm       <= #1 2'b11;
        data      <= #1 data;
        ready     <= #1 1'b0;

        ctr_reset <= #1 1'b0;
      end
      //CAS - 1 NOP 
      8'b00010000: begin
        command   <= #1 4'b0111;
        address   <= #1 13'b0000000000000;
        bank      <= #1 2'b00;
        dqm       <= #1 2'b00;
        data      <= #1 data;
        ready     <= #1 1'b0;

        ctr_reset <= #1 1'b0;
      end
      //CAS - 2 NOP 
      8'b00100000: begin
        command   <= #1 4'b0111;
        address   <= #1 13'b0000000000000;
        bank      <= #1 2'b00;
        dqm       <= #1 2'b00;
        data      <= #1 data;
        ready     <= #1 1'b0;

        ctr_reset <= #1 1'b1;
      end
      //READING - 8
      8'b01000000: begin
        command   <= #1 4'b0111;
        address   <= #1 13'b0000000000000;
        bank      <= #1 2'b00;
        dqm       <= #1 dqm_count ? 2'b00 : 2'b11;
        data      <= #1 ((data << 16) | {112'b0, DRAM_DQ});
        ready     <= #1 1'b0;

        ctr_reset <= #1 1'b0;
      end
      //NOP - FIN
      8'b10000000: begin
        command   <= #1 4'b0111;
        address   <= #1 13'b0000000000000;
        bank      <= #1 2'b00;
        dqm       <= #1 2'b11;
        data      <= #1 data;
        ready     <= #1 1'b1;

        ctr_reset <= #1 1'b0;
      end
    endcase
  end

endmodule

// ----------------------------------------------------------- Write
module sdram_write (
    input  iclk,
    input  ireset,
    input  ireq,
    input  ienb,
    output ofin,

    input [ 12:0] irow,
    input [  9:0] icolumn,
    input [  1:0] ibank,
    input [127:0] idata,

    output        DRAM_CLK,
    output        DRAM_CKE,
    output [12:0] DRAM_ADDR,
    output [ 1:0] DRAM_BA,
    output        DRAM_CAS_N,
    output        DRAM_CS_N,
    output        DRAM_RAS_N,
    output        DRAM_WE_N,
    output        DRAM_LDQM,
    output        DRAM_UDQM,
    output [15:0] DRAM_DQ
);

  reg  [  6:0] state = 7'b0000001;
  reg  [  6:0] next_state;

  reg  [  3:0] command = 4'h0;
  reg  [ 12:0] address = 13'h0;
  reg  [  1:0] bank = 2'b00;
  reg  [127:0] data = 128'h0;
  reg  [  1:0] dqm = 2'b11;

  reg          ready = 1'b0;

  reg  [  7:0] counter = 8'h0;
  reg          ctr_reset = 0;

  wire         data_count;

  assign ofin                                           = ready;

  assign DRAM_ADDR                                      = ienb ? address : {13{1'bz}};
  assign DRAM_BA                                        = ienb ? bank : 2'bzz;
  assign {DRAM_CS_N, DRAM_RAS_N, DRAM_CAS_N, DRAM_WE_N} = ienb ? command : 4'bzzzz;
  assign {DRAM_UDQM, DRAM_LDQM}                         = ienb ? dqm : 2'bzz;
  assign DRAM_CLK                                       = ienb ? ~iclk : 1'bz;
  assign DRAM_CKE                                       = ienb ? 1'b1 : 1'bz;
  assign DRAM_DQ                                        = ienb ? data[127:112] : {16{1'bz}};

  always @(posedge iclk or posedge ctr_reset) begin
    if (ctr_reset) counter <= #1 8'h0;
    else counter <= #1 (counter + 1'b1);
  end

  assign data_count = (counter == 5);

  always @(posedge iclk) begin
    if (ireset == 1'b1) state <= #1 7'b0000001;
    else state <= #1 next_state;
  end

  always @(state or ireq or data_count) begin
    case (state)
      //IDLE
      7'b0000001: if (ireq) next_state <= 7'b0000010;
 else next_state <= 7'b0000001;
      //ACTIVE
      7'b0000010: next_state <= 7'b0000100;
      //NOP
      7'b0000100: next_state <= 7'b0001000;
      //WRITE
      7'b0001000: next_state <= 7'b0010000;
      //WRITING
      7'b0010000: if (data_count) next_state <= 7'b0100000;
 else next_state <= 7'b0010000;
      //NOP
      7'b0100000: next_state <= 7'b1000000;
      //NOP - FIN
      7'b1000000: next_state <= 7'b0000001;
      default:    next_state <= 7'b0000001;
    endcase
  end

  always @(posedge iclk) begin
    case (state)
      //IDLE
      default,
      7'b0000001: begin
        command   <= #1 4'b0111;
        address   <= #1 13'b0000000000000;
        bank      <= #1 2'b00;
        dqm       <= #1 2'b11;
        data      <= #1 data;
        ready     <= #1 1'b0;

        ctr_reset <= #1 1'b0;
      end
      //ACTIVE
      7'b0000010: begin
        command   <= #1 4'b0011;
        address   <= #1 irow;
        bank      <= #1 ibank;
        dqm       <= #1 2'b11;
        data      <= #1 idata;
        ready     <= #1 1'b0;

        ctr_reset <= #1 1'b0;
      end
      //NOP
      7'b0000100: begin
        command   <= #1 4'b0111;
        address   <= #1 13'b0000000000000;
        bank      <= #1 2'b00;
        dqm       <= #1 2'b11;
        data      <= #1 data;
        ready     <= #1 1'b0;

        ctr_reset <= #1 1'b1;
      end
      //WRITE
      7'b0001000: begin
        command   <= #1 4'b0100;
        address   <= #1{3'b001, icolumn};
        bank      <= #1 ibank;
        dqm       <= #1 2'b00;
        data      <= #1 data;
        ready     <= #1 1'b0;

        ctr_reset <= #1 1'b1;
      end
      //WRITING  
      7'b0010000: begin
        command   <= #1 4'b0111;
        address   <= #1 13'b0000000000000;
        bank      <= #1 2'b00;
        dqm       <= #1 2'b00;
        data      <= #1 (data << 16);
        ready     <= #1 1'b0;

        ctr_reset <= #1 1'b0;
      end
      //NOP
      7'b0100000: begin
        command   <= #1 4'b0111;
        address   <= #1 13'b0000000000000;
        bank      <= #1 2'b00;
        dqm       <= #1 2'b11;
        data      <= #1 data;
        ready     <= #1 1'b0;

        ctr_reset <= #1 1'b0;
      end
      //NOP - FIN
      7'b1000000: begin
        command   <= #1 4'b0111;
        address   <= #1 13'b0000000000000;
        bank      <= #1 2'b00;
        dqm       <= #1 2'b11;
        data      <= #1 data;
        ready     <= #1 1'b1;

        ctr_reset <= #1 1'b0;
      end
    endcase
  end

endmodule

